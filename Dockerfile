FROM node:current-alpine

RUN apk update && \
    apk add bash git openssh-client curl make gcc g++ binutils-gold python3 linux-headers paxctl libgcc libstdc++ git perl --no-cache && \
    npm install -g gulp karma nodemon bower